SRCS=main.s slow_status_flash.s flash_button.s swi_handler.s framebuffer.s
OBJS=main.o slow_status_flash.o flash_button.o swi_handler.o framebuffer.o

all: kernel.img

kernel.img: kernel.elf
	arm-none-eabi-objcopy kernel.elf -O binary kernel.img

kernel.elf: ${OBJS} linkscript.ld
	arm-none-eabi-ld --no-undefined ${OBJS} -o kernel.elf -T linkscript.ld

%.o: src/%.s
	arm-none-eabi-as -Isrc $< -o $@

clean:
	rm -f ${OBJS} kernel.elf kernel.img
