/*
 * flash_button.s
 *
 * Copyright © 2018 Thomas White <taw@bitwiz.org.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

.include "swi_numbers.h"

.global flash_button

flash_button:
	MOV	R0, #0			@ Get GPLEV0
	SWI	OS_GetGPIO
	TST	R0, #1<<17		@ Check GPIO 17
	BLNE	flash_leds
	B	flash_button

flash_leds:
	STMDB	R13!, {LR}

	MOV	R2, #5			@ Number of times to flash

1:
	MOV	R0, #0			@ Flash first set of GPIOs

	MOV	R1, #1<<16
	SWI	OS_ClearGPIO
	MOV	R1, #1<<18
	SWI	OS_SetGPIO

	BL	pause

	MOV	R1, #1<<18
	SWI	OS_ClearGPIO
	MOV	R1, #1<<16
	SWI	OS_SetGPIO

	BL	pause

	SUBS	R2, R2, #1
	BNE	1b

	MOV	R1, #1<<18
	ORR	R1, #1<<16
	SWI	OS_ClearGPIO

	LDMIA	R13!, {PC}

pause:
	MOV	R1, #0x3f000
1:
	SUBS	R1, R1, #1
	BNE	1b
	MOV	PC, LR
