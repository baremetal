/*
 * swi_handler.s
 *
 * Copyright © 2018 Thomas White <taw@bitwiz.org.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

.include "swi_numbers.h"

.global swi_handler

swi_handler:
	STMDB	R13!, { R12, LR}

	LDR	R12, [LR, #-4]
	BIC	R12, R12, #0xff000000

	CMP	R12, #OS_GetGPIO
	BEQ	get_gpio

	CMP	R12, #OS_SetGPIO
	BEQ	set_gpio

	CMP	R12, #OS_ClearGPIO
	BEQ	clear_gpio

	CMP	R12, #OS_GetCPUID
	BEQ	get_cpu_id

	LDMIA	R13!, {R12, PC}^

get_gpio:
	STMDB	R13!, {R8}
	LDR	R8, =0x3f200000
	LDR	R0, [R8, #0x34]		@ GPLEV0
	LDMIA	R13!, {R8}
	LDMIA	R13!, {R12, PC}^

set_gpio:
	STMDB	R13!, {R8}
	LDR	R8, =0x3f200000
	STR	R1, [R8, #0x1c]		@ GPSET0
	LDMIA	R13!, {R8}
	LDMIA	R13!, {R12, PC}^

clear_gpio:
	STMDB	R13!, {R8}
	LDR	R8, =0x3f200000
	STR	R1, [R8, #0x28]		@ GPCLR0
	LDMIA	R13!, {R8}
	LDMIA	R13!, {R12, PC}^

get_cpu_id:
	MRC	P15, 0, R0, C0, C0, 5
	AND	R0, R0, #0x0f
	LDMIA	R13!, {R12, PC}^

