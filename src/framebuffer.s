/*
 * framebuffer.s
 *
 * Copyright © 2018 Thomas White <taw@bitwiz.org.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

.include "ports.h"

.global framebuffer_init

framebuffer_init:
	STMDB	R13!, {R14}

	ADR	R1, framebuffer_info
	ADD	R1, R1, #0x40000000
	MOV	R0, #1
	BL	send_mailbox
@	BL	read_mailbox

	CMP	R1, #0
	LDMNEIA	R13!, {PC}	@ Fail

	LDMIA	R13!, {PC}


/* ->  R0 = mailbox number
 *     R1 = value to write */
send_mailbox:
	STMDB	R13!, {R2, R8, R14}
	LDR	R8, =(PERIPHERAL_BASE+OFFS_MAILBOX)

	/* Wait for empty mailbox */
1:
	LDR	R2, [R8, #18]
	TST	R2, #1<<31
	BNE	1b

	AND	R2, R0, #0xf
	ORR	R2, R2, R1
	STR	R2, [R8, #20]

	LDMIA	R13!, {R2, R8, PC}


/* -> R0 = mailbox number
 * <- R1 = value read */
read_mailbox:
	STMDB	R13!, {R2, R8, R14}
	LDR	R8, =(PERIPHERAL_BASE+OFFS_MAILBOX)

	/* Wait for non-empty mailbox */
1:
	LDR	R2, [R8, #18]
	TST	R2, #1<<30
	BNE	1b

	LDR	R2, [R8, #0]
	AND	R1, R2, #0xf
	CMP	R1, R0
	BNE	1b		@ Wrong mailbox

	BIC	R1, R2, #0xf

	LDMIA	R13!, {R2, R8, PC}


.align 4
framebuffer_info:
	.int	1024
	.int	768
	.int	1024
	.int	768
	.int	0
	.int	16
	.int	0
	.int	0
	.int	0
	.int	0
