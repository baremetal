/*
 * slow_status_flash.s
 *
 * Copyright © 2018 Thomas White <taw@bitwiz.org.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

.include "swi_numbers.h"

.global flash_status_led

flash_status_led:

forever:
	SWI	OS_GetCPUID
	MOV	R7, R0
	MOV	R6, R7
	MOV	R0, #0
1:
	MOV	R1, #1<<29
	SWI	OS_SetGPIO
	BL	longpause
	MOV	R1, #1<<29
	SWI	OS_ClearGPIO
	BL	longpause
	SUBS	R6, R6, #1
	BNE	1b

	BL	longpause
	BL	longpause
	BL	longpause
	B	forever

longpause:
	MOV	R2, #0x3f0000
2:
	SUBS	R2, R2, #1
	BNE	2b
	MOV	PC, LR
